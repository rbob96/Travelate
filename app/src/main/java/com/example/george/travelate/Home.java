package com.example.george.travelate;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Home extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener
{

    private GoogleMap mMap;
    Button cafeButton;
    Button barButton;
    Button restaurantButton;
    public static final String LOCATION = "com.example.george.travelate.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        cafeButton = (Button) findViewById(R.id.cafeButton);
        barButton = (Button) findViewById(R.id.barButton);
        restaurantButton = (Button) findViewById(R.id.restaurantButton);

        cafeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Home.this, DisplayLocationActivity.class);
                String location = "cafe";
                intent.putExtra(LOCATION, location);
                startActivity(intent);
            }
        });

        barButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Home.this, DisplayLocationActivity.class);
                String location = "bar";
                intent.putExtra(LOCATION, location);
                startActivity(intent);
            }
        });

        restaurantButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Home.this, DisplayLocationActivity.class);
                String location = "restaurant";
                intent.putExtra(LOCATION, location);
                startActivity(intent);
            }
        });
    }
    //Static tourist locations in Paris, Berlin, Tokyo, Glasgow including cafes, bars and food
    private static final LatLng berlinGendarmenmarket = new LatLng(52.5137,13.3927) ;
    private static final LatLng berlinCheckpointCharlie = new LatLng(52.5074,13.3904) ;
    private static final LatLng berlinHamburgerBahnhof = new LatLng(52.5284,13.3721) ;
    private static final LatLng berlinZoo = new LatLng(52.5073,13.3323) ;

    private Marker mBerlinGendar;
    private Marker mBerlinCheck;
    private Marker mBerlinBahnhof;
    private Marker mBerlinZoo;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a markers and move to Berlin
        mBerlinGendar = mMap.addMarker(new MarkerOptions()
                .position(berlinGendarmenmarket)
                .title("Gendarmenmarket"));
        mBerlinGendar.setTag(0);

        mBerlinCheck = mMap.addMarker(new MarkerOptions()
                .position(berlinCheckpointCharlie)
                .title("Checkpoint Charlie"));
        mBerlinCheck.setTag(0);

        mBerlinZoo = mMap.addMarker(new MarkerOptions()
                .position(berlinZoo)
                .title("Berlin Zoo"));
        mBerlinZoo.setTag(0);

        mBerlinBahnhof = mMap.addMarker(new MarkerOptions()
                .position(berlinHamburgerBahnhof)
                .title("Hamburger Bahnhof"));
        mBerlinBahnhof.setTag(0);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(berlinGendarmenmarket));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12),2000, null);

        mMap.setOnMarkerClickListener(this);
    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        if (marker != null){
            Intent intent = new Intent(Home.this, DisplayLocationActivity.class);
            String location = marker.getTitle();
            intent.putExtra(LOCATION, location);
            startActivity(intent);
        }
        return false;
    }
}
