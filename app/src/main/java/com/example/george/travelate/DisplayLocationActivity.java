package com.example.george.travelate;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Button;

import java.util.Locale;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class DisplayLocationActivity extends AppCompatActivity {

    private static final String API_KEY = "AIzaSyDq92Ut_7CwOEkjeFfRrjxuMFwKJH0he10";
    TextToSpeech tts;
    String[] gendarmenmarket = {"How much does this cost?", "Can I have a taste?","Do you have this in other colours?"};
    String[] checkpointcharlie = {"Can I take pictures?","Where can I get my passport stamped?", "Where can I read more about it?"};
    String[] berlinzoo = {"When is the last entrance?","One ticket please.", "Can I get a map of the zoo?", "Can I feed the animals?" };
    String[] hamburger = {"Can I take pictures?", "How much does a ticket cost?","Do you have any student tickets?"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_location);

        Intent intent = getIntent();
        String location = intent.getStringExtra(Home.LOCATION);

        TextView locationTitle = findViewById(R.id.locationTitle);
        locationTitle.setText(location);

        ScrollView scrollView = (ScrollView)findViewById(R.id.scrollView);
        LinearLayout phrasesLayout = (LinearLayout)findViewById(R.id.phrasesLayout);

        tts = new TextToSpeech(DisplayLocationActivity.this, new TextToSpeech.OnInitListener(){@Override
        public void onInit(int status) {
        }
        });
        tts.setLanguage(Locale.GERMAN);

         TextView locationDescription = (TextView)findViewById(R.id.locationDescription);
         ImageView locationImage = (ImageView)findViewById(R.id.locationImage);

        if (location.equals("Gendarmenmarket"))
        {
            locationDescription.setText("Gendarmenmarkt is one of the most beautiful squares in Berlin, with the Konzerthaus, Deutscher Dom and Französischer Dom. Find out more at visitBerlin.de.");
            locationImage.setBackgroundResource(R.drawable.download);
            DisplayLocationActivity.this.addButtons(phrasesLayout, gendarmenmarket);
        }
        else if (location.equals("Checkpoint Charlie"))
        {
            locationDescription.setText("Checkpoint Charlie was the name given by the Western Allies to the best-known Berlin Wall crossing point between East Berlin and West Berlin during the Cold War.");
            locationImage.setBackgroundResource(R.drawable.checkpointcharlie);
            DisplayLocationActivity.this.addButtons(phrasesLayout,checkpointcharlie);
        }
        else if (location.equals("Berlin Zoo"))
        {
            locationDescription.setText("Zoo Berlin is Germany's oldest zoological garden and home to the world's largest variety of species. Almost 20,000 animals of around 1,400 species live in the 33-hectare zoo. They include elephants, giraffes, gorillas, and Germany's only giant pandas.");
            locationImage.setBackgroundResource(R.drawable.berlinzoo);
            DisplayLocationActivity.this.addButtons(phrasesLayout,berlinzoo);
        }
        else if (location.equals("Hamburger Bahnhof"))
        {
            locationDescription.setText("The Hamburger Bahnhof – Museum für Gegenwart – Berlin presides over a comprehensive collection of contemporary art, which it presents in a variety of exhibitions.");
            locationImage.setBackgroundResource(R.drawable.hamburger);
            DisplayLocationActivity.this.addButtons(phrasesLayout,hamburger);
        }

        int height_in_pixels = locationDescription.getLineCount() * locationDescription.getLineHeight(); //approx height text
        locationDescription.setHeight(height_in_pixels);


        final TextInputEditText customPhrase = (TextInputEditText)findViewById(R.id.customInput);
        final Button customTranslate = (Button)findViewById(R.id.customTranslate);
        customTranslate.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                DisplayLocationActivity.this.translateCustomInput(customPhrase);
            }
        });

    }

    @Override
    protected void onDestroy() {


        //Close the Text to Speech Library
        if(tts != null) {

            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    public void translate (final Button b){
        final String buttonText = b.getText().toString();
        final Handler buttonHandler = new Handler();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                TranslateOptions options = TranslateOptions.newBuilder()
                        .setApiKey(API_KEY)
                        .build();
                Translate translate = options.getService();
                final Translation translation =
                        translate.translate(buttonText,
                                Translate.TranslateOption.targetLanguage("de"));
                buttonHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        b.setText(translation.getTranslatedText());
                        tts.speak(translation.getTranslatedText(), TextToSpeech.QUEUE_ADD, null);
                    }
                });
                return null;
            }
        }.execute();
    }

    public void translateCustomInput(final TextInputEditText customInput){
        final String customPhrase = customInput.getEditableText().toString();
        final Handler inputHandler = new Handler();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                TranslateOptions options = TranslateOptions.newBuilder()
                        .setApiKey(API_KEY)
                        .build();
                Translate translate = options.getService();
                final Translation translation =
                        translate.translate(customPhrase,
                                Translate.TranslateOption.targetLanguage("de"));
                inputHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        customInput.setText(translation.getTranslatedText());
                        tts.speak(translation.getTranslatedText(), TextToSpeech.QUEUE_ADD, null);
                    }
                });
                return null;
            }
        }.execute();

    }

    public void addButtons(LinearLayout phrasesLayout, String[] phrases)
    {
        for (String phrase : phrases){
            final Button usefulPhrase = new Button(this);
            usefulPhrase.setText(phrase);

            usefulPhrase.setOnClickListener (new Button.OnClickListener(){
                @Override
                public void onClick(View v){
                    DisplayLocationActivity.this.translate(usefulPhrase);
                }
            });

            phrasesLayout.addView(usefulPhrase);
        }
    }
}
